
Feature: Example Feature

    @example=get
    Scenario Outline: Example Scenario
        Given user makes a get request to "v1/quote"
        * whit a request query including the below details
            | scale             | <scale>       |
            | state             | NSW           |
            | paymentFrequency  | Weekly        |
            | excess            | 500.00        |
            | rebateTier        | Tier0         |
            | dob               | 05-10-1970    |
            | previousCover     | false         |
            | hospitalProduct   | q_hospital_1  |
            | extrasProduct     | q_extras_1    |
            | effectiveDate     | 31-03-2018    |
        When response status is "200"
        Then validates response has valid quote total of "<price>"

        Examples:
        | scale                 | price |
        | Single                | 31.86 |
        | SingleParentFamily    | 53.72 |
        | Family                | 58.72 |
        | Couple                | 55.37 |

    @example=post
    Scenario: validate offers for all products
      Quote for NSW Single Weekly 500.00 excess Tier0 from 10/10/1950
      effective date 01-01-2018
        Given user makes a post request to "v1/quote" with payload "hospital-only"
        When response status is "200"
        Then I can validate the prices below
            | q_hospital_1          | 32.39 |
            | q_hospital_2          | 46.32 |
            | q_hospital_3          | 59.55 |
            | q_hospital_4          | 73.46 |
            | hospital_basic_plus   | 34.75 |