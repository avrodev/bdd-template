package utils;

public class UserData {
    private String offerCode;
    private String passes;

    public UserData(String offerCode, String passes) {
        this.offerCode = offerCode;
        this.passes = passes;
    }

    public String getOfferCode(){
        return offerCode;
    }

    public String getPasses() {
        return passes;
    }
}
