package cucumberSteps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import library.CommonEvents;
import library.RestAssuredEvents;
import library.S3FileUploader;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.stream.Stream;

import static java.nio.file.StandardOpenOption.*;
/*import static java.nio.file.StandardOpenOption.TRUNCATE_EXISTING;
import static java.nio.file.StandardOpenOption.WRITE;*/

public class TestData {

      S3FileUploader fileUpload = new S3FileUploader();
    @Given("^create \"([^\"]*)\" qff$")
    public void createQff(int numberOfQff) throws IOException, InterruptedException {
        String heading = "qff,title,firstname,lastname,pin,regoNumber,address,emailAddress,mobileNumber,memberId,dob,commencementDate";
        File file = new File("src/test/resources/payloads/newqffs.csv");
        File adrsFile = new File("src/test/resources/payloads/adrs.txt");
        List<RegoAndAddress> regoAndAddress = new ArrayList<>();
        File regoFile = new File("src/test/resources/payloads/rego.txt");
        try (Stream<String> stream = Files.lines(Paths.get(String.valueOf(regoFile)))) {
            stream.forEach(rego -> {

                try (Stream<String> stream2 = Files.lines(Paths.get(String.valueOf(adrsFile)))) {
                    stream2.forEach(address -> {
                        RegoAndAddress regoAndAddres = new RegoAndAddress();
                        regoAndAddres.rego = rego;
                        regoAndAddres.address = address;
                        regoAndAddress.add(regoAndAddres);
                    });

                } catch (IOException e) {
                    e.printStackTrace();
                }

            });
        }

        // file.delete();
        Files.write(Paths.get(String.valueOf(file)), (heading + System.lineSeparator()).getBytes(), CREATE, APPEND);
        try {
            for (int i = 0; i < numberOfQff; i++) {

                String qffNumber = CommonEvents.createQff();
                String iLoyal = CommonEvents.getIloyal(qffNumber);
                Random random = new Random();

                int x = random.nextInt(350);
                Files.write(Paths.get(String.valueOf(file)), (qffNumber + ","
                        + "MR" + ","
                        + CommonEvents.firstName + ","
                        + "loytest" + ","
                        + "0246" + ","
                        + regoAndAddress.get(x).rego + ","
                        + regoAndAddress.get(x).address + ","
                        + CommonEvents.email + ","
                        + "0473048569" + ","
                        + iLoyal + ","
                        + "1981-10-16" + ","
                        + "2019-10-10"
                        + System.lineSeparator()).getBytes(), CREATE, APPEND);
            }
            //fileUpload.uploadFilesToS3("src/test/resources/payloads/", "file-transfer-integration-test", "logs");
        } catch (Exception e) {
            //     fileUpload.uploadFilesToS3("src/test/resources/payloads/", "file-transfer-integration-test", "logs");
        }
    }

    @Then("^get iLoyal id$")
    public void get_iLoyal() throws IOException, InterruptedException {

    }

    @Given("^create \"([^\"]*)\" qffs with motor$")
    public void createQffwithMotor(int numberOfQff) throws IOException, InterruptedException {

        try {
            for (int i = 0; i < numberOfQff; i++) {

                String qffNumber = CommonEvents.createQff();
                String iLoyal = CommonEvents.getIloyal(qffNumber);
                String body = CommonEvents.prepareEventBody(qffNumber,iLoyal);
                System.out.println("payload:: "+ " "+ body);
                fileUpload.sendMessageToQueue(body);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
