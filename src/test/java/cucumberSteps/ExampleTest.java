package cucumberSteps;

import com.jayway.restassured.response.Response;
import library.CommonEvents;
import library.RestAssuredEvents;
import utils.UserData;
import cucumber.api.java.en.*;
import cucumber.api.*;
import org.json.JSONException;
import org.junit.Assert;
import org.skyscreamer.jsonassert.JSONAssert;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.*;


public class ExampleTest {

    private RestAssuredEvents rest = new RestAssuredEvents();
    private Response response;
    private CommonEvents com = new CommonEvents();
    private String request;

   List<UserData> data  = new ArrayList<>();

    @Given("^user makes a get request to \"([^\"]*)\"$")
    public void user_makes_a_get_request_to(String quote) throws IOException {
        request = (quote);
    }

    @Given("^whit a request query including the below details$")
    public void whit_a_request_query_including_the_below_details(DataTable quoteQuery) throws IOException {
        // loop through and add = + &
        // should look like http://internal-uat-quote-403144962.ap-southeast-2.elb.amazonaws.com:8080/v1/quote?scale=Single&state=NSW&paymentFrequency=Weekly&excess=500.00&rebateTier=Tier0&dob=05-10-1970&previousCover=false&hospitalProduct=q_hospital_1&extrasProduct=q_extras_1
        List<List<String>> data = quoteQuery.raw();
        String query = "?";
        for(int i=0; i<data.size(); i++){
//            System.out.println(data.get(i).get(0)); // <--- enable for trouble shooting if needed
//            System.out.println(data.get(i).get(1)); // <--- enable for trouble shooting if needed
            query = query + data.get(i).get(0) + "=" + data.get(i).get(1) + "&";
        }
        query = query.substring(0, query.length() - 1);
//        System.out.println(request + query); // <--- enable for trouble shooting if needed
        response = rest.getRequest(request + query);
    }

    @Given("^user makes a post request to \\\"(.*?)\\\" with payload \\\"(.*?)\\\"$")
    public void postRequest(String path, String fileName) throws IOException {
        String body = com.read("src/test/resources//payloads/" + fileName + ".json");
        response = rest.postRequest(path, body);
    }

    @When("^response status is \"([^\"]*)\"$")
    public void response_status_is(String statusCode) throws IOException {
        response.then().statusCode(Integer.parseInt(statusCode));
    }

    @Then("^validates response has valid quote total of \"([^\"]*)\"$")
    public void validates_response_has_valid_quote_total_of(String quoteTotalPrice) throws IOException {
        response.then().assertThat().body("quote.total",equalTo(quoteTotalPrice));
    }


    @Then("^I can validate the prices below$")
    public void validate_price_list(DataTable PriceList) throws IOException {
        // loop through and add = + &
        // should look like http://internal-uat-quote-403144962.ap-southeast-2.elb.amazonaws.com:8080/v1/quote?scale=Single&state=NSW&paymentFrequency=Weekly&excess=500.00&rebateTier=Tier0&dob=05-10-1970&previousCover=false&hospitalProduct=q_hospital_1&extrasProduct=q_extras_1
        List<List<String>> data = PriceList.raw();
        for(int i=0; i<data.size(); i++){
//            System.out.println(data.get(i).get(0)); // <--- enable for trouble shooting if needed
//            System.out.println(data.get(i).get(1)); // <--- enable for trouble shooting if needed
            response.then().assertThat().body("quote."+data.get(i).get(0),equalTo(data.get(i).get(1)));
        }
    }
}

