//package com.qantasloyalty.lsl;

import com.jayway.restassured.RestAssured;
import library.CommonEvents;
import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        format = {"progress"},
        features = {"src/test/resources/features"},
        tags = {"@run","~@regression", "~@ignore"}
)
public class TestRunner {
    @BeforeClass
    public static void envSetup() {
        final EnvironmentVariables variables = new SystemEnvironmentVariables();
        CommonEvents.setEnv(variables);
        RestAssured.baseURI = CommonEvents.get("endpointQuote");
    }

}
