package library;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.MessageAttributeValue;
import com.amazonaws.services.sqs.model.SendMessageRequest;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class S3FileUploader {
    public final AmazonS3 s3Client = AmazonS3ClientBuilder.defaultClient();

    public void uploadFilesToS3(String sourceFileFolder,String bucketName, String bucketFolder) {

        String objectKeyPrefix =  bucketFolder.endsWith("/")?bucketFolder:bucketFolder+"/" ;

        final File folder = new File(sourceFileFolder);

        for (File csvFile : folder.listFiles()) {
            if (csvFile.getName().endsWith(".csv")) {
                String fileName = csvFile.getName();
                putFileToS3(csvFile,bucketName, objectKeyPrefix+fileName);
            }
        }
    }

    private void putFileToS3(File csvFile,String bucketName, String objectKey) {
        ObjectMetadata objectMetadata = new ObjectMetadata();

        PutObjectRequest putRequest =
                new PutObjectRequest(bucketName, objectKey, csvFile);
        putRequest.setMetadata(objectMetadata);
        PutObjectResult result = s3Client.putObject(putRequest);
        System.out.println(result.toString());

    }

    public void sendMessageToQueue(String body){
        Map<String, MessageAttributeValue> messageAttributes = new HashMap<>();
        messageAttributes.put("eventType",new MessageAttributeValue()
                         .withStringValue("motor-application-policy-purchase")
                         .withDataType("String"));
        AmazonSQS sqs = AmazonSQSClientBuilder.defaultClient();
        String queueUrl = sqs.getQueueUrl("avro-uat-integration-motor-policy-service-events-incoming").getQueueUrl();
        SendMessageRequest sendMessageRequest = new SendMessageRequest().withQueueUrl(queueUrl)
                                                               .withMessageBody(body)
                                                                .withMessageAttributes(messageAttributes).withDelaySeconds(3);
        sqs.sendMessage(sendMessageRequest);
    }

}
