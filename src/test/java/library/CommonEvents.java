package library;

import com.google.common.base.Strings;
import com.jayway.restassured.response.Response;
import net.thucydides.core.util.EnvironmentVariables;
import org.apache.commons.lang3.RandomStringUtils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.Properties;
import java.util.stream.Stream;


public class CommonEvents {
    public static String firstName;
    public static String email;
    private static Properties environmentProperties = new Properties();

    public static void setEnv(final EnvironmentVariables environmentVariables ){
        setEnv(
                Stream.of(
                        environmentVariables.getProperty("runEnv"),
                        environmentVariables.getValue("runEnv")
                )
                        .filter(Objects::nonNull).map(Strings::emptyToNull)
                        .findFirst()
                        .orElse("uat")
        );
    }
    private static void setEnv(final String runEnv) {
        final String envPropertiesFile = String.format("src/test/resources/%s.properties", runEnv.toLowerCase());
        try (final InputStream propertiesFile = new FileInputStream(envPropertiesFile)) {
            environmentProperties.load(propertiesFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Properties Loaded");
        System.out.println(environmentProperties);
    }
    public static String get(final String property) {
        return environmentProperties.getProperty(property);
    }

    //for connecting to Dynamo DB
    public static String getEnvPrefix(){
        String env = System.getProperty("runEnv");
        if(env==null){
            env = "dev";
        }
        String envPrefix = "avro-"+env+"-integration-";
        System.out.print("LOADING " + envPrefix);
        return envPrefix.toLowerCase();
    }

    public static String read(String filepath) throws IOException {
        return new String(Files.readAllBytes(Paths.get(filepath)), StandardCharsets.UTF_8);
    }

    public static String createQff() throws IOException, InterruptedException {
        String body = CommonEvents.read("src/test/resources/payloads/join-qff.json");
        firstName = RandomStringUtils.randomAlphabetic(8);
        email = firstName+"loytest@yopmail.com";
        String jsonBody = body.replace("fname", firstName);
        String emailBody = jsonBody.replace("emailad",email);
        String request = CommonEvents.get("endpointJoin");
        Response response = RestAssuredEvents.postRequest(request, emailBody);
        return RestAssuredEvents.getResponseAsJsonPath("qffNumber");
    }
    public static String getIloyal(String qffNumber) throws IOException, InterruptedException {
        String request = "http://internal-uat-identity-reference-180490586.ap-southeast-2.elb.amazonaws.com:8080/v1/identityreference/QFF/"+qffNumber;
        Thread.sleep(5000);
        Response response = RestAssuredEvents.getRequest(request);
        return RestAssuredEvents.getResponseAsJsonPath("identityReferences[0].identityId");
    }

    public static String prepareEventBody(String qffNumber, String iLoyal) throws IOException {
        String policyNumber = RandomStringUtils.randomNumeric(9);
        String applId = "QM"+policyNumber;
        String body = CommonEvents.read("src/test/resources/payloads/motor-event.json");
        String jsonBody1 = body.replace("Qa",firstName);
        String jsonBody2 = jsonBody1.replace("replaceemail",email);
        String jsonBody3 = jsonBody2.replace("replaceiLoyal",iLoyal);
        String jsonBody4 = jsonBody3.replace("replaceApplId",applId);
        String postBody = jsonBody4.replace("replacePolicyNumber",policyNumber);
        return postBody;

    }








}
