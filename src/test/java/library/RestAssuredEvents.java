package library;

import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;

import static com.jayway.restassured.RestAssured.given;

public class RestAssuredEvents {
private static Response response;

    public static Response getRequest(String uri){
        response = given().relaxedHTTPSValidation().when().get(uri);
        return response;
    }

    public static Response postRequest(String path, String body){
        response = given()
                .relaxedHTTPSValidation()
                .header("Content-Type", "application/json;charset=UTF-8")
                .contentType(ContentType.JSON)
                .body(body)
                .when().post(path);
        return response;
    }
    public static String getResponseAsJsonPath(String path){
        JsonPath jp = new JsonPath(response.asString());//.setRoot(root);
        String result = jp.get(path).toString();
        return result;
    }



}
