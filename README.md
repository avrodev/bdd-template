#AVRO LSL BDD TEMPLATE


##How to use this template

- Copy this Repository and replace the word TEMPLATE and template with the name of your service or test suite
- There are some files and config so you can run the test but you must be in the Qantas Loyalty VPN to be able to successfully run it
- update the config base URL at /src/test/java/config.properties

## prerequisites
Java
apache maven installed
set java home and m2 home in system env variables

## build
mvn clean verify

## run in cli
mvn clean verify -DrunEnv=UAT /DEV /STG
if run env not specified default is UAT

## reporting
using serenity BDD the report will be generated after execution at below location
/target/site/serenity/index.html

## Example Features and Scenarios

Feature: Example Feature

    @tag=example
    Scenario Outline: Example Scenario
        Given user makes a get request to "v1/quote"
        * whit a request query including the below details
            | scale             | <scale>       |
            | state             | NSW           |
            | paymentFrequency  | Weekly        |
            | excess            | 500.00        |
            | rebateTier        | Tier0         |
            | dob               | 05-10-1970    |
            | previousCover     | false         |
            | hospitalProduct   | q_hospital_1  |
            | extrasProduct     | q_extras_1    |
            | effectiveDate     | 31-03-2018    |
        When response status is "200"
        Then validates response has valid quote total of "<price>"

        Examples:
        | scale                 | price |
        | Single                | 31.86 |
        | SingleParentFamily    | 53.72 |
        | Family                | 58.72 |
        | Couple                | 55.37 |