#!/usr/bin/env bash

curl -X POST \
  https://hooks.slack.com/services/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 \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: 16ae36b5-58e2-7007-546a-d870f439a71f' \
  -d '{
    "text": "TEMPLATE BDD test are failing - <https://devops-jenkins.qantasassure.com/job/lsl-assure/job/bdd-TEMPLATE/|Click here> for more details",
    "username": "Bug Hunter",
    "icon_emoji": ":bughunter:"
}'